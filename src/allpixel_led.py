#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bibliopixel.drivers.serial_driver import *
from bibliopixel import LEDStrip
import bibliopixel.colors as PixelColors
from bibliopixel.animation import BaseStripAnim

#import the module you'd like to use
from BiblioPixelAnimations.strip import Rainbows
from BiblioPixelAnimations.strip import HalvesRainbow
from BiblioPixelAnimations.strip import ColorFade
from BiblioPixelAnimations.strip import ColorChase
from BiblioPixelAnimations.strip import LarsonScanners

import bibliopixel.log as pixellog
import logging

import math, sys

import roslib; roslib.load_manifest('hmri_leds')
import rospy

from std_msgs.msg import ColorRGBA
from hmri_leds.msg import LedCmd

class Police(BaseStripAnim):
  def __init__(self, led, colors = [PixelColors.Red, PixelColors.Blue], start=0, end=-1):
    if start == -1:
      start = led.numLEDs
    if end == -1:
      end = led.numLEDs

    if abs(end) - abs(start) > 0:
      self.direction = 1
    else:
      self.direction = -1
      (start, end) = (end, start)

    #The base class MUST be initialized by calling super like this
    super(Police, self).__init__(led, start, end)
    #Create a color array to use in the animation
    self._colors = colors#[PixelColors.Red, PixelColors.Black, PixelColors.Blue, PixelColors.Black]
    if not len(self._colors):
      self._cstrip_len = self._size
    else:
      self._cstrip_len = int(self._size / len(self._colors))

  def step(self, amt = 1):
    self._led.all_off()
    #Fill the strip, with each sucessive color
    for i in range(self._start, self._end + 1):
      self._led.set(i, self._colors[(self._step - i * self.direction) % self._size / self._cstrip_len - 1])
    #Increment the internal step by the given amount
    self._step += amt

class LedFlasher(object):
  """ROS interface to AllPixel board via BiblioPixel library"""
  def __init__(self):
    """Constructor"""

    # Init ROS node
    rospy.init_node("led_flasher")

    self.num_leds = rospy.get_param("~num_leds", 36)
    if self.num_leds < 0:
      rospy.logerr("Wrong number of leds specified: " + str(self.num_leds))
      sys.exit(1)

    self.max_brightness = rospy.get_param("~max_brightness", 32)
    if self.max_brightness < 0 or self.max_brightness > 255:
      rospy.logerr("Wrong maximum brightness specified: " + str(self.max_brightness)
        + ". Valid range is [0 - 255]")
      sys.exit(1)

    c_order = rospy.get_param("~channel_order", "bgr")
    if hasattr(ChannelOrder, c_order.upper()):
      self.channel_order = getattr(ChannelOrder, c_order.upper())
    else:
      rospy.logerr("Wrong channel order specified: " + c_order
        + ". Valid values are [RGB, RBG, GRB, GBR, BRG, BGR]")
      sys.exit(1)

    self.startup_anim = rospy.get_param("~startup_anim", 0)


    # Register on shutdown callback to switch off the LEDs
    rospy.on_shutdown(self.on_shutdown)

    # Replace BiblioPixel log handlers with rosout
    while len(pixellog.logger.handlers):
      h = pixellog.logger.handlers[len(pixellog.logger.handlers) - 1]
      pixellog.logger.removeHandler(h)

    for h in logging.getLogger("rosout").handlers:
      pixellog.logger.addHandler(h)

    # Set pixel log level to INFO
    #pixellog.setLogLevel(pixellog.INFO)

    #init driver with the type and count of LEDs you're using
    self.driver = DriverSerial(type = LEDTYPE.APA102, num = self.num_leds, c_order = self.channel_order)

    #init controller
    self.led = LEDStrip(self.driver, masterBrightness = self.max_brightness)


    self.sub_leds = rospy.Subscriber("led_cmd", LedCmd, self.led_cmd_cb)

    self.anim = None

    if self.startup_anim:
        msg = LedCmd()
        msg.type = self.startup_anim
        msg.colors = [ColorRGBA(1.0, 1.0, 0.0, 0.0)]
        self.led_cmd_cb(msg)

    # self.select_animation =
    # {
    #   LedCmd.OFF:      self.switch_off,
    #   LedCmd.RAINBOW:  self.animation_rainbow,
    #   LedCmd.LARSON:   self.animation_larson,
    #   LedCmd.FADE:     self.animation_fade
    # }

  def saturate(self, val, min, max):
    if val < min:
      return min
    if val > max:
      return max
    return val

  def on_shutdown(self):
    self.switch_off();
    rospy.loginfo("Exiting...")

  def rgb_list_to_tuples(self, colors):
    rgb_tuples = []

    for c in colors:
      r = self.saturate(int(c.r * 255.0), 0, 255);
      g = self.saturate(int(c.g * 255.0), 0, 255);
      b = self.saturate(int(c.b * 255.0), 0, 255);

      rgb_tuples.append((r, g, b))

    return rgb_tuples

  def animation_rainbow(self, colors = [], start = 0, end = -1):
    if len(colors) > 0:
      rospy.logwarn("Animation Rainbow does not require any colors")
    self.anim = HalvesRainbow.HalvesRainbow(self.led)

  def animation_larson(self, colors = [PixelColors.Magenta], start = 0, end = -1):
    if len(colors) > 1:
      rospy.logwarn("Animation LarsonScanner requires only one color, but you supplied " + str(len(colors)))
    if not len(colors):
      colors = [PixelColors.Magenta]
    self.anim = LarsonScanners.LarsonScanner(self.led, colors[0], tail = 4, start = start, end = end)

  def animation_fade(self, colors = [PixelColors.Red, PixelColors.Green, PixelColors.Blue], start = 0, end = -1):
    if not len(colors):
      rospy.logwarn("Animation ColorFade requires at least one color")
    if not len(colors):
      colors = [PixelColors.Red, PixelColors.Green, PixelColors.Blue]
    self.anim = ColorFade.ColorFade(self.led, colors, start = start, end = end)

  def animation_police(self, colors = [PixelColors.Red, PixelColors.Blue], start = 0, end = -1):
    if not len(colors):
      colors = [PixelColors.Red, PixelColors.Blue]
    self.anim = Police(self.led, colors, start = start, end = end)

  def switch_off(self):
    if self.anim:
      self.anim.stopThread(True) # Wait animation is over
      self.anim = None

    # Turn off LEDs
    self.led.all_off()
    self.led.update()

  def on_anim_complete(self, anim):
    # if self.anim:
    #   self.anim.animComplete = True
    pass

  def led_cmd_cb(self, msg):
    if msg.type != LedCmd.OFF:
      # Convert ColorRGBA array to tuple array
      colors = self.rgb_list_to_tuples(msg.colors)

      # Stop previous animation
      if self.anim:
        self.anim.stopThread(True)

      if msg.brightness == 0: msg.brightness = self.max_brightness
      self.led.setMasterBrightness(msg.brightness)

      if (msg.start == msg.end == 0):
        msg.end = self.led.numLEDs

      if msg.type == LedCmd.RAINBOW:
        self.animation_rainbow(colors, msg.start, msg.end)
      if msg.type == LedCmd.LARSON:
        self.animation_larson(colors, msg.start, msg.end)
      if msg.type == LedCmd.FADE:
        self.animation_fade(colors, msg.start, msg.end)
      if msg.type == LedCmd.POLICE:
        self.animation_police(colors, msg.start, msg.end)

    else: # LedCmd.OFF
      self.switch_off()
      return

    #run the animation with default 50 frames per second
    if msg.fps <= 0.0 or math.isnan(msg.fps):
      rospy.logwarn("LedCmd.fps is <= 0.0 or NaN, using default [50 Hz]")
      fps = 50.0
    elif math.isinf(msg.fps):
      rospy.logwarn("LedCmd.fps is Inf, animation will run as fast as possible")
      fps = None
    else:
      fps = msg.fps

    self.anim.run(fps = fps, untilComplete = True, threaded = True,
      callback = self.on_anim_complete)

  def run(self):
    rospy.spin()

if __name__ == '__main__':
  leds = LedFlasher()

  try:
    leds.run()
  except rospy.ROSInterruptException:
    leds.switch_off()
